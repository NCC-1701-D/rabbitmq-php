# RabbitMQ - Tutorials

## Development

### Setting up dev environment

1. Set up environment variables:

        cp .env.dist .env

2. Run docker containers:

        docker-compose up -d --build
    
3. Log into rabbitmq-php container:

        docker exec -it rabbitmq-php /bin/bash

4. Install dependencies(in container):

        composer install